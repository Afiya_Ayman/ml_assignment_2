import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
import tensorflow as tf
import keras
from keras.layers.core import Activation
from keras.models import Sequential,load_model
from keras.layers import Dense, Dropout, LSTM,SimpleRNN
from keras.callbacks import ModelCheckpoint
from keras.callbacks import LambdaCallback
from keras.callbacks import Callback
import collections
from keras import optimizers
import csv
import random


from numpy import loadtxt
DataSet = loadtxt('DataSet/pima-indians-diabetes.data.csv',delimiter=',')





def unit_vector(vector):
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))



Tupple = np.shape(DataSet)
Number_of_Records = Tupple[0]
Number_of_Features = Tupple[1]

print(f'Number_of_Records = {Number_of_Records}')
print(f'Number_of_Features = {Number_of_Features}')


Sample_Inputs = np.zeros((Number_of_Records, Number_of_Features - 1))

for t in range(Number_of_Records):
    Sample_Inputs[t] = DataSet[t, :(Number_of_Features - 1)]

print(f'Input_Size = {np.shape(Sample_Inputs)}')

#Target feature 'house price of unit area' as labels
Sample_Label = np.zeros((Number_of_Records,))
for t in range(Number_of_Records):
    Sample_Label[t] = DataSet[t][Number_of_Features-1]

print(f'Label_Size = {np.shape(Sample_Label)}')


print('*******************************')


X = Sample_Inputs
y = Sample_Label



################

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

print(f'Training_Input = {np.shape(X_train)}')
print(f'Training_Labels = {np.shape(y_train)}')

print(f'Testing_inputs = {np.shape(X_test)}')
print(f'Testing_Labels = {np.shape(y_test)}')



#Training Model

Number_of_layers = 3
model = Sequential()
model.add(Dense(12, input_dim=8, activation='sigmoid'))
model.add(Dense(10, activation='sigmoid'))
model.add(Dense(8, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))



for n in range(0,Number_of_layers):
    print("Weights: ")
    print(np.shape(model.layers[n].get_weights()[0]))
    print("Biases: ")
    print(np.shape(model.layers[n].get_weights()[1]))


weights = []
history = []

class SaveWeights(Callback):
    def on_epoch_end(self, epoch, logs=None):
        global model
        weights.append({
            'layer_0': model.layers[0].get_weights()[0],
            'layer_1': model.layers[1].get_weights()[0],
            'layer_2': model.layers[2].get_weights()[0],
            'layer_3': model.layers[3].get_weights()[0]
        })
        history.append(logs)


# Compile the keras model
optimizer = keras.optimizers.sgd(lr=0.001,momentum=0.001)
# optimizer = keras.optimizers.adam(lr=0.01)
model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'],)



# fit the keras model on the train dataset
model.fit(X_train, y_train, epochs=1000, batch_size=32,
                    verbose=1,
                    validation_data=(X_test, y_test),callbacks=[SaveWeights()])




#######################

#Activation
from keras import backend as K

Output_1 = []
Output_2 = []
Output_3 = []
Output_4 = []
for i in range(Number_of_layers):
    OutFunc = K.function([model.input], [model.layers[i].output])
    if i == 0:
        Output_1 = OutFunc([X_train])[0]
    if i == 1:
        Output_2 = OutFunc([X_train])[0]
    if i == 2:
        Output_3 = OutFunc([X_train])[0]
    if i == 3:
        Output_4 = OutFunc([X_train])[0]


# print(np.shape(Output_1))
# print(np.shape(Output_2))
# print(np.shape(Output_3))
# print(np.shape(Output_4))


def get_Activation(N,Output_Layer):
    Neuron = N
    Output_from_Neuron = []
    Frequency = {}

    for i in Output_Layer:
        Output_from_Neuron.append(i[Neuron-1])

    ctr = collections.Counter(Output_from_Neuron)
    ctr = dict(ctr)
    # print(ctr)
    # print(sum(ctr.values()))


    i = 0.1
    while (i <= 1):
        c = 0
        current_i = round(i, 2)
        prev_i = i - 0.1
        Frequency[0] = c
        Frequency[current_i] = c
        Frequency[1] = c
        for key, value in ctr.items():
            if key == 0:
                Frequency[0] = value
            if current_i >= key > prev_i:
                c = c + value
            if key == 1:
                Frequency[1] = value

        Frequency[current_i] = c

        i = i + 0.1

    # print(sum(Frequency.values()))
    # print(Frequency)
    return Frequency


def plot_Activation(X, Y_1, Y_2,Hidden):
    f = plt.figure()
    ax = f.add_subplot(121)
    y_pos = np.arange(len(X))
    plt.bar(y_pos, Y_1)
    plt.xticks(y_pos, X, size=13)
    # plt.yscale('log')
    plt.ylabel('Number of Samples', size=13)
    plt.xlabel('Activation Value', size=13)
    plt.title(f"Activation Output from Neuron 1 : Hidden Layer {Hidden}", size=13)

    ax = f.add_subplot(122)
    y_pos = np.arange(len(X))
    plt.bar(y_pos, Y_2)
    plt.xticks(y_pos, X, size=13)
    # plt.yscale('log')
    plt.ylabel('Number of Samples', size=13)
    plt.xlabel('Activation Value', size=13)
    plt.title(f"Activation Output from Neuron 2 : Hidden Layer {Hidden}", size=13)
    plt.show()


#1st Hidden Layer
Neuron = 1
Layer = 1
Activation_Neuron_1_Hidden_1 = get_Activation(Neuron,Output_1)
# print(sum(Activation_Neuron_1_Hidden_1.values()))
X = sorted(Activation_Neuron_1_Hidden_1.keys())
Y_11 = []
for key, value in sorted(Activation_Neuron_1_Hidden_1.items()):
    Y_11.append(value)


Neuron = 2
Layer = 1
Activation_Neuron_2_Hidden_1 = get_Activation(Neuron,Output_1)
# print(sum(Activation_Neuron_2_Hidden_1.values()))

Y_21 = []
for key, value in sorted(Activation_Neuron_2_Hidden_1.items()):
    Y_21.append(value)

print(Activation_Neuron_1_Hidden_1)
print(Activation_Neuron_2_Hidden_1)
print(len(X))
print(len(Y_11))
print(len(Y_21))

# Plot Activation
Hidden = 1
plot_Activation(X, Y_11, Y_21, Hidden)




#2nd Hidden Layer
Neuron = 1
Layer = 2
Activation_Neuron_1_Hidden_2 = get_Activation(Neuron,Output_2)
# print(sum(Activation_Neuron_1_Hidden_2.values()))
X = sorted(Activation_Neuron_1_Hidden_2.keys())
Y_12 = []
for key, value in sorted(Activation_Neuron_1_Hidden_2.items()):
    Y_12.append(value)


Neuron = 2
Layer = 2
Activation_Neuron_2_Hidden_2 = get_Activation(Neuron,Output_2)
# print(sum(Activation_Neuron_2_Hidden_2.values()))

Y_22 = []
for key, value in sorted(Activation_Neuron_2_Hidden_2.items()):
    Y_22.append(value)


# Plot Activation
Hidden = 2
plot_Activation(X, Y_12, Y_22,Hidden)

#3rd Hidden Layer
Neuron = 1
Layer = 3
Activation_Neuron_1_Hidden_3 = get_Activation(Neuron,Output_3)
# print(sum(Activation_Neuron_1_Hidden_3.values()))
X = sorted(Activation_Neuron_1_Hidden_3.keys())
Y_13 = []
for key, value in sorted(Activation_Neuron_1_Hidden_3.items()):
    Y_13.append(value)


Neuron = 2
Layer = 3
Activation_Neuron_2_Hidden_3 = get_Activation(Neuron,Output_3)
# print(sum(Activation_Neuron_2_Hidden_3.values()))

Y_23 = []
for key, value in sorted(Activation_Neuron_2_Hidden_3.items()):
    Y_23.append(value)


# Plot Activation
Hidden = 3
plot_Activation(X, Y_13, Y_23,Hidden)


with open('DataSet/Activation_report.csv', 'w') as report_file:
    csv_writer = csv.writer(report_file)
    csv_writer.writerow(['Activation_Values',
                         'Activation_Neuron1_Hidden1', 'Activation_Neuron2_Hidden1',
                         'Activation_Neuron1_Hidden2', 'Activation_Neuron2_Hidden2',
                         'Activation_Neuron1_Hidden3', 'Weight_Neuron2_Hidden3'])
    for i in range(len(X)):
        csv_writer.writerow([
            X[i],
            Y_11[i],Y_21[i],
            Y_12[i],Y_22[i],
            Y_13[i],Y_23[i]
        ])





###########

#Weights



#For Layer 0


Neuron_1_layer_0 = []

Neuron_2_layer_0 = []

#For Layer 1


Neuron_1_layer_1 = []

Neuron_2_layer_1 = []

#For Layer 2

Neuron_1_layer_2 = []

Neuron_2_layer_2 = []

for i in range(len(weights)):
    for key,value in weights[i].items():
        if key=='layer_0':
            vector_10 = []
            vector_20 = []
            for j in value:
                vector_10.append(j[0])
                vector_20.append(j[1])
        if key == 'layer_1':
            vector_11 = []
            vector_21 = []
            for j in value:
                vector_11.append(j[0])
                vector_21.append(j[1])
        if key == 'layer_2':
            vector_12 = []
            vector_22 = []
            for j in value:
                vector_12.append(j[0])
                vector_22.append(j[1])
    Neuron_1_layer_0.append(vector_10)
    Neuron_2_layer_0.append(vector_20)
    Neuron_1_layer_1.append(vector_11)
    Neuron_2_layer_1.append(vector_21)
    Neuron_1_layer_2.append(vector_12)
    Neuron_2_layer_2.append(vector_22)

ANGLES_Neuron_1_Layer_0 = []
ANGLES_Neuron_2_Layer_0 = []
ANGLES_Neuron_1_Layer_1 = []
ANGLES_Neuron_2_Layer_1 = []
ANGLES_Neuron_1_Layer_2 = []
ANGLES_Neuron_2_Layer_2 = []

for i in range(len(Neuron_1_layer_0)-1):
    ANGLES_Neuron_1_Layer_0.append(angle_between(Neuron_1_layer_0[i],Neuron_1_layer_0[i+1]))
    ANGLES_Neuron_2_Layer_0.append(angle_between(Neuron_2_layer_0[i],Neuron_2_layer_0[i+1]))

for i in range(len(Neuron_1_layer_1)-1):
    ANGLES_Neuron_1_Layer_1.append(angle_between(Neuron_1_layer_1[i],Neuron_1_layer_1[i+1]))
    ANGLES_Neuron_2_Layer_1.append(angle_between(Neuron_2_layer_1[i],Neuron_2_layer_1[i+1]))

for i in range(len(Neuron_1_layer_2)-1):
    ANGLES_Neuron_1_Layer_2.append(angle_between(Neuron_1_layer_2[i],Neuron_1_layer_2[i+1]))
    ANGLES_Neuron_2_Layer_2.append(angle_between(Neuron_2_layer_2[i],Neuron_2_layer_2[i+1]))

print(np.shape(ANGLES_Neuron_1_Layer_0))

f = plt.figure()
ax=f.add_subplot(121)
plt.plot(ANGLES_Neuron_1_Layer_0)
plt.title('Weights for a Neuron-1 on 1st Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')

ax=f.add_subplot(122)
plt.plot(ANGLES_Neuron_2_Layer_0)
plt.title('Weights for a Neuron-2 on 1st Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')
plt.show()

f = plt.figure()
ax=f.add_subplot(121)
plt.plot(ANGLES_Neuron_1_Layer_1)
plt.title('Weights for a Neuron-1 on 2nd Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')

ax=f.add_subplot(122)
plt.plot(ANGLES_Neuron_2_Layer_1)
plt.title('Weights for a Neuron-2 on 2nd Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')
plt.show()

f = plt.figure()
ax=f.add_subplot(121)
plt.plot(ANGLES_Neuron_1_Layer_2)
plt.title('Weights for a Neuron-1 on 3rd Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')

ax=f.add_subplot(122)
plt.plot(ANGLES_Neuron_2_Layer_2)
plt.title('Weights for a Neuron-2 on 3rd Hidden Layer')
plt.ylabel('Angle between the weight vectors')
plt.xlabel('Iteration')
plt.show()

def smooth(plot, weight = .90, sample_size=1):
    ret = []
    smoothed = plot[0]

    for i in range(len(plot)):
        smoothed = smoothed * weight + plot[i] * (1-weight)
        ret.append(smoothed)

    return ret#random.sample(ret, int(sample_size * len(plot)))



with open('DataSet/report.csv', 'w') as report_file:
    csv_writer = csv.writer(report_file)
    csv_writer.writerow(['Iteration',
                         'Weight_Neuron1_Hidden1', 'Weight_Neuron2_Hidden1',
                         'Weight_Neuron1_Hidden2', 'Weight_Neuron2_Hidden2',
                         'Weight_Neuron1_Hidden3', 'Weight_Neuron2_Hidden3',
                         'Accuracy', 'Val_Acc', 'Loss', 'Val_Loss'])

    report = {
        'ANGLES_Neuron_1_Layer_0': smooth(ANGLES_Neuron_1_Layer_0),
        'ANGLES_Neuron_2_Layer_0': smooth(ANGLES_Neuron_2_Layer_0),
        'ANGLES_Neuron_1_Layer_1': smooth(ANGLES_Neuron_1_Layer_1),
        'ANGLES_Neuron_2_Layer_1': smooth(ANGLES_Neuron_2_Layer_1),
        'ANGLES_Neuron_1_Layer_2': smooth(ANGLES_Neuron_1_Layer_2),
        'ANGLES_Neuron_2_Layer_2': smooth(ANGLES_Neuron_2_Layer_2),
        'acc': smooth([h['acc'] for h in history]),
        'val_acc': smooth([h['val_acc'] for h in history]),
        'loss': smooth([h['loss'] for h in history]),
        'val_loss': smooth([h['val_loss'] for h in history]),
    }


    for i in range(len(report['ANGLES_Neuron_1_Layer_0'])):
        csv_writer.writerow([
            i,
            report['ANGLES_Neuron_1_Layer_0'][i],
            report['ANGLES_Neuron_2_Layer_0'][i],
            report['ANGLES_Neuron_1_Layer_1'][i],
            report['ANGLES_Neuron_2_Layer_1'][i],
            report['ANGLES_Neuron_1_Layer_2'][i],
            report['ANGLES_Neuron_2_Layer_2'][i],
            report['acc'][i],
            report['val_acc'][i],
            report['loss'][i],
            report['val_loss'][i]
        ])





# evaluate the keras model
scores = model.evaluate(X_train, y_train, verbose=1)
Loss = scores[0]
print(f"Loss : {Loss}")
Accuracy = scores[1]
print(f'Accuracy: %.2f' % (Accuracy*100))

predictions = model.predict_classes(X_test)


# list all data in history

# summarize history for accuracy
plt.plot([h['acc'] for h in history])
plt.plot([h['val_acc'] for h in history])
plt.title('Model Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Iteration')
plt.legend(['Train', 'Test'], loc='upper left')
# plt.savefig('Acc_10k.png', dpi=850)

# summarize history for loss
plt.plot([h['loss'] for h in history])
plt.plot([h['val_loss'] for h in history])
plt.title('Model Loss')
plt.ylabel('Loss')
plt.xlabel('Iteration')
plt.legend(['Train', 'Test'], loc='upper left')
# plt.savefig('loss_10k.png', dpi=850)
#
plt.show()