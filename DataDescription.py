import pandas as pd

columns = ['Number of times pregnant',
              'Plasma glucose concentration a 2 hours in an oral glucose tolerance test',
              'Diastolic blood pressure (mm Hg)',
              'Triceps skin fold thickness (mm)',
              '2-Hour serum insulin (mu U/ml)',
             'Body mass index (weight in kg/(height in m)$^2$)',
             'Diabetes pedigree function',
             'Age (years)','Diabetic']

df = pd.read_csv('DataSet/pima-indians-diabetes.data.csv',names=columns)
for col in df.columns:
    print(col)

print(df.head())


Data_Description = df.describe()
Data_Description.to_csv('Data_Description.csv')

